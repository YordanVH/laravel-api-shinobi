<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Validator;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class UserController extends BaseController
{
	public function index()
    {
        $users = User::all();

        return $this->returnResponse($users->toArray(), 'Users returned successfully.');
    }

    public function create(Request $request)

    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
            'role' => 'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $user->assignRole($input['role']);

        return $this->sendResponse($user->toArray(), 'User created successfully.');
    }

    public function update(Request $request, $id)
    {
    	$input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user = User::find($id);
        $input['password'] = bcrypt($input['password']);
        $user->name = $input['name'];
        $user->password = $input['password'];
        $user->save();

        return $this->sendResponse($user->toArray(), 'User updated successfully.');
    }

    public function destroy($id)
    {
    	$user = User::findOrFail($id);
    	$user->delete();

    	return $this->sendResponse([], 'User delete successfully.');
    }

    public function createRole(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $role = Role::create($input);

        return $this->sendResponse($role->toArray(), 'Role created successfully.');
    }

    public function destroyRole($id)
    {
    	$role = Role::findOrFail($id);
    	$role->delete();

    	return $this->sendResponse([], 'Role delete successfully.');
    }

    public function createPermission(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $permission = Permission::create($input);

        return $this->sendResponse($permission->toArray(), 'Permission created successfully.');
    }

    public function destroyPermission($id)
    {
    	$permission = Permission::findOrFail($id);
    	$permission->delete();

    	return $this->sendResponse([], 'Permission delete successfully.');
    }

    public function assignRole($userId, $roleId)
    {

    	$user = User::find($userId);
    	$user->assignRole($roleId);
    	$user = User::find($userId);
    	$roles = $user->getRoles();
    	return json_encode($roles);
    }

    public function revokeRole($userId, $roleId)
    {

    	$user = User::find($userId);
    	$user->revokeRole($roleId);
		$user = User::find($userId);
    	$roles = $user->getRoles();
    	return json_encode($roles);
    }

    public function assignPermission($roleId, $permissionId)
    {
    	$role = Role::find($roleId);
    	$role->assignPermission($permissionId);
    	$role = Role::find($roleId);
    	$permissions = $role->getPermissions();
    	return json_encode($permissions);
    }

    public function revokePermission($roleId, $permissionId)
    {
    	$role = role::find($roleId);
    	$role->revokePermission($permissionId);
		$role = Role::find($roleId);
    	$permissions = $role->getPermissions();
    	return json_encode($permissions);
    }

    public function revokeAllPermissions($roleId)
    {
    	$role = role::find($roleId);
    	$role->revokeAllPermissions();
		$role = Role::find($roleId);
    	$permissions = $role->getPermissions();
    	return json_encode($permissions);
    }

}
