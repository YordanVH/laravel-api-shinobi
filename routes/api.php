<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group(['middleware' => 'auth:api'], function() {

        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::get('users', 'UserController@index');

        Route::group(['middleware' => 'has.role:client'], function(){

			Route::group(['prefix' => 'user'], function () {
				Route::post('create', 'UserController@create');
			    Route::put('update/{id}', 'UserController@update');
			    Route::delete('delete/{id}', 'UserController@destroy');
			    Route::post('{userId}/assign-role/{roleId}', 'UserController@assignRole');
			    Route::post('{userId}/revoke-role/{roleId}', 'UserController@revokeRole');
			    Route::post('create-role', 'UserController@createRole');
			    Route::post('create-permission', 'UserController@createPermission');
			});

	        Route::post('role/{roleId}/assign-permission/{permissionId}', 'UserController@assignPermission');
	        Route::post('role/{roleId}/revoke-permission/{permissionId}', 'UserController@revokePermission');
	        Route::delete('role/delete/{id}', 'UserController@destroyRole');
	        Route::delete('permission/delete/{id}', 'UserController@destroyPermission');
	        Route::post('role/{roleId}/delete-all-permissions', 'UserController@revokeAllPermissions');
    	});
    });
});